Приведен просто скрипт без файлов

Данный скрипт подготовлен для работы без прав root (под УЗ "freeswich") и не на системном диске.
Перед запуском нужно узнать имя каталога, куда примонтирован дополнительный диск и изменить переменную в скрипте. В данном примере у нас диск sdb1 примонтирован к каталогу /u01.

Для запуска необходимо:

Залить архив на будущий сервер
Разархивировать его командой tar -xvf fs-bin-current-version_4.tar.gz
Перейти в распакованный каталог cd fs-bin-current-version_4
Запустить скрипт установки ./fs-install_v4.sh
В конце будет вывод с информацией о запуске freeswich