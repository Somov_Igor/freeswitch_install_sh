#!/bin/sh

echo "create user "freeswitch" and add /bin/systemctl..."
adduser freeswitch
echo "freeswitch ALL=NOPASSWD: /bin/systemctl start freeswitch.service, /bin/systemctl stop freeswitch.service, /bin/systemctl restart freeswitch.service, /bin/systemctl reload freeswitch.service, /bin/systemctl status freeswitch.service, /usr/bin/sngrep" > /etc/sudoers.d/freeswitch
echo "**************************"

hdd=u01
echo "freeswitch apps location will be in $hdd"
echo "**************************"

echo "create directories in $hdd..."
mkdir -p /$hdd/freeswitch/{log,run,bin}
echo "**************************"

echo "copying files..."
cp -R ./usr/* /$hdd/freeswitch/
echo "**************************"

echo "set permissions for user "freeswitch"..."
chmod -R u=rwx,g=rx,o=r /$hdd/freeswitch/
chmod -x /$hdd/freeswitch/lib/systemd/system/freeswitch.service
chown -R freeswitch:freeswitch /$hdd/freeswitch/
echo "**************************"

echo "create symlinks..."
ln -s /$hdd/freeswitch/lib/systemd/system/freeswitch.service /usr/lib/systemd/system
ln -s /$hdd/freeswitch /usr/local/freeswitch
ln -s /$hdd/freeswitch/local/freeswitch/bin/fs_cli /usr/bin/fs_cli
ln -s /$hdd/freeswitch/local/freeswitch/bin/freeswitch /$hdd/freeswitch/bin/freeswitch
echo "**************************"

echo "create symlinks /usr/lib64/libraries..."
check_ln_lib64() {
	if [ $# -gt 0 ]
	then
		if [ -f /usr/lib64/$1 ]
		then
			echo -n $1
			echo " exist"
		else
			ln -s /$hdd/freeswitch/lib64/$1 /usr/lib64/$1
			echo -n $1
			echo " link created"
			if [ $# -eq 2 ]
			then
				ln -s /usr/lib64/$1 /usr/lib64/$2
				echo -n $2
				echo " link created"
			fi
		fi
	fi
}
check_ln_lib64 libFLAC.so.8.3.0 libFLAC.so.8
check_ln_lib64 libcodecpro.so
check_ln_lib64 libgsm.so.1.0.12 libgsm.so.1
check_ln_lib64 libjbig.so.2.0
check_ln_lib64 libjbig85.so.2.0
check_ln_lib64 libldns.so.1.6.16 libldns.so.1
check_ln_lib64 libmp3lame.so.0.0.0 libmp3lame.so.0
check_ln_lib64 libmpg123.so.0.44.5 libmpg123.so.0
check_ln_lib64 libodbc.so.2.0.0 libodbc.so.2
check_ln_lib64 libogg.so.0.8.0 libogg.so.0
check_ln_lib64 libopus.so.0.5.0 libopus.so.0
check_ln_lib64 libpq.so.5.5 libpq.so.5
check_ln_lib64 libshout.so.3.2.0 libshout.so.3
check_ln_lib64 libsndfile.so.1.0.25 libsndfile.so.1
check_ln_lib64 libspeex.so.1.5.0 libspeex.so.1
check_ln_lib64 libspeexdsp.so.1.5.0 libspeexdsp.so.1
check_ln_lib64 libtheora.so.0.3.10 libtheora.so.0
check_ln_lib64 libtiff.so.5.2.0 libtiff.so.5
check_ln_lib64 libvorbis.so.0.4.6 libvorbis.so.0
check_ln_lib64 libvorbisenc.so.2.0.9 libvorbisenc.so.2
check_ln_lib64 libvorbisfile.so.3.3.5 libvorbisfile.so.3
check_ln_lib64 libfreeswitch.so.1.0.0 libfreeswitch.so.1
check_ln_lib64 libspandsp.so.3.0.0 libspandsp.so.3
check_ln_lib64 libsofia-sip-ua.so.0.6.0 libsofia-sip-ua.so.0
echo "**************************"

echo "create symlinks /usr/local/bin..."
check_ln_bin() {
	if [ $# -gt 0 ]
	then
		if [ -f /usr/local/bin/$1 ]
		then
			echo -n $1
			echo " exist"
		else
			ln -s /$hdd/freeswitch/local/bin/$1 /usr/local/bin/$1
			echo -n $1
			echo " link created"
			if [ $# -eq 2 ]
			then
				ln -s /$hdd/freeswitch/local/bin/$1 /usr/local/bin/$2
				echo -n $2
				echo " link created"
			fi
		fi
	fi
}
check_ln_bin addrinfo
check_ln_bin chardetect
check_ln_bin freeswitch
check_ln_bin freeswitch_exporter
check_ln_bin localinfo
check_ln_bin sip-date
check_ln_bin sip-dig
check_ln_bin sip-options
check_ln_bin stunc
echo "**************************"

echo "create symlinks /usr/local/lib/libraries..."
check_ln_lib() {
	if [ $# -gt 0 ]
	then
		if [ -f /usr/local/lib/$1 ]
		then
			echo -n $1
			echo " exist"
		else
			ln -s /$hdd/freeswitch/local/lib/$1 /usr/local/lib/$1
			echo -n $1
			echo " link created"
			if [ $# -eq 2 ]
			then
				ln -s /$hdd/freeswitch/local/lib/$1 /usr/local/lib/$2
				echo -n $2
				echo " link created"
			fi
		fi
	fi
}
check_ln_lib libsofia-sip-ua.a
check_ln_lib libsofia-sip-ua.la
check_ln_lib libsofia-sip-ua.so.0.6.0
check_ln_lib libspandsp.a
check_ln_lib libspandsp.la
check_ln_lib libspandsp.so.3.0.0 
echo "**************************"

echo "set control groups changes..."
echo 950000 > /sys/fs/cgroup/cpu/cpu.rt_runtime_us
echo "**************************"

echo "create link for log dir..."
ln -s /$hdd/freeswitch/log /var/log/freeswitch
echo "**************************"

echo "installing additional packages..."
yum install -y ./libjpeg-turbo-1.2.90-8.el7.x86_64.rpm
yum install -y ./sngrep/sngrep-1.4.9-0.el7.x86_64.rpm
yum install -y ./mc/mc-4.8.7-11.el7.x86_64.rpm
echo "**************************"

echo "copy fs_cli config..."
cp ./fs_cli.conf /etc/fs_cli.conf
sleep 1
echo "**************************"

echo "starting service..."
systemctl enable freeswitch
echo "**************************"
sleep 5
systemctl start freeswitch
echo "**************************"
sleep 5
systemctl status freeswitch
